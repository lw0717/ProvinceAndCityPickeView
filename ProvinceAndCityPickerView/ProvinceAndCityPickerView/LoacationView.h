//
//  LoacationView.h
//  ProvinceAndCityPickerView
//
//  Created by Apple on 14/12/3.
//  Copyright (c) 2014年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^SelectedCity)(NSString * cityStr);
typedef void (^CancelSelect)(void);

@interface LoacationView : UIView<UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIPickerView *locationPickView;

@property (nonatomic, copy) SelectedCity selectCityBlock;
@property (nonatomic, copy) CancelSelect cancelSelectBlock;

- (void)haveSelectedCity:(SelectedCity)tempSelectCityBlock;
- (void)haveCancelSelect:(CancelSelect)tempCancelSelectBlock;

- (instancetype)initWithFrame:(CGRect)frame andDataDic:(NSMutableDictionary *)dic;
- (void)reloadLocationPickerViewWithPickerArray:(NSMutableArray *)array
                                 subPickerArray:(NSMutableArray *)tempArray
                               thirdPickerArray:(NSMutableArray *)tempThirdArray
                                 andStringArray:(NSMutableArray *)tempStringArray;

@end
