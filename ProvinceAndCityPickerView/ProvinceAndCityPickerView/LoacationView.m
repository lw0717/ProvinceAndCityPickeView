//
//  LoacationView.m
//  ProvinceAndCityPickerView
//
//  Created by Apple on 14/12/3.
//  Copyright (c) 2014年 Apple. All rights reserved.
//

#import "LoacationView.h"

@interface LoacationView ()

@property (nonatomic, strong) NSMutableDictionary * dict;
@property (nonatomic, strong) NSMutableArray * pickerArray; // 省
@property (nonatomic, strong) NSMutableArray * subPickerArray; // 市
@property (nonatomic, strong) NSMutableArray * thirdPickerArray; // 区
@property (nonatomic, strong) NSMutableArray * selectArray;

@property (nonatomic, strong) NSString * provinceString;
@property (nonatomic, strong) NSString * cityString;
@property (nonatomic, strong) NSString * areaString;

@end

@implementation LoacationView

#define FirstComponent 0
#define SubComponent 1
#define ThirdComponent 2

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupSubviews];
        
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame andDataDic:(NSMutableDictionary *)dic
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"LocationView" owner:self options:nil] objectAtIndex:0];
    self.frame = frame;
    
    if (self) {
        
        self.dict = [NSMutableDictionary dictionaryWithDictionary:dic];
        self.pickerArray = [[NSMutableArray alloc] initWithArray:[_dict allKeys]];
        self.selectArray = [[NSMutableArray alloc] initWithArray:[_dict objectForKey:[[_dict allKeys] objectAtIndex:0]]];
        if ([_selectArray count] > 0) {
            self.subPickerArray = [[NSMutableArray alloc] initWithArray:[[self.selectArray objectAtIndex:0] allKeys]];
        }
        
        if ([_subPickerArray count] > 0) {
            self.thirdPickerArray = [[NSMutableArray alloc] initWithArray:[[self.selectArray objectAtIndex:0] objectForKey:[self.subPickerArray objectAtIndex:0]]];
        }
        self.provinceString = [self.pickerArray objectAtIndex:0];
        self.cityString = [self.subPickerArray objectAtIndex:0];
        self.areaString = [self.thirdPickerArray objectAtIndex:0];
        [self setupSubviews];

    }
    return self;
}

- (void)setupSubviews
{
    self.locationPickView.delegate = self;
    self.locationPickView.dataSource = self;
    self.locationPickView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - reloadDataWith Array
- (void)reloadLocationPickerViewWithPickerArray:(NSMutableArray *)array subPickerArray:(NSMutableArray *)tempArray thirdPickerArray:(NSMutableArray *)tempThirdArray andStringArray:(NSMutableArray *)tempStringArray
{
    self.pickerArray = array;
    self.subPickerArray = tempArray;
    self.thirdPickerArray = tempThirdArray;
    self.provinceString = [tempStringArray objectAtIndex:0];
    self.cityString = [tempStringArray objectAtIndex:1];
    self.areaString = [tempStringArray objectAtIndex:2];
    [self.locationPickView reloadAllComponents];
}


#pragma mark -
#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case FirstComponent:
        {
            return [self.pickerArray count];
        }
            break;
        case SubComponent:
        {
            return [self.subPickerArray count];
        }
            break;
        case ThirdComponent:
        {
            return [self.thirdPickerArray count];
        }
            break;
            
        default:
            break;
    }
    return 0;
}

#pragma mark -
#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case FirstComponent:
        {
            return [self.pickerArray objectAtIndex:row];
        }
            break;
        case SubComponent:
        {
            return [self.subPickerArray objectAtIndex:row];
        }
            break;
        case ThirdComponent:
        {
            return [self.thirdPickerArray objectAtIndex:row];
        }
            break;
            
        default:
            break;
    }
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == FirstComponent) {
        self.provinceString = [self.pickerArray objectAtIndex:row];
        self.selectArray = [_dict objectForKey:[self.pickerArray objectAtIndex:row]];
        if (self.selectArray.count > 0) {
            self.subPickerArray = (NSMutableArray *)[[self.selectArray objectAtIndex:0] allKeys];
            
        } else {
            self.subPickerArray = nil;
        }
        
        if (self.subPickerArray.count > 0) {
            self.thirdPickerArray = [[self.selectArray objectAtIndex:0] objectForKey:[self.subPickerArray objectAtIndex:0]];
        } else {
            self.thirdPickerArray = nil;
        }
        
        
//        [pickerView selectedRowInComponent:1];
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        
        self.cityString = [self.subPickerArray objectAtIndex:0];
        
        
//        [pickerView selectedRowInComponent:2];
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
        self.areaString = [self.thirdPickerArray objectAtIndex:0];
        
    }
    
    if (component == SubComponent) {
        self.cityString = [self.subPickerArray objectAtIndex:row];
        
        if (self.selectArray.count > 0 && self.subPickerArray.count > 0) {
            self.thirdPickerArray = [[self.selectArray objectAtIndex:0] objectForKey:[self.subPickerArray objectAtIndex:row]];
        } else {
            self.thirdPickerArray = nil;
        }
        [pickerView selectRow:0 inComponent:2 animated:YES];
        [pickerView reloadComponent:2];
        
        self.areaString = [self.thirdPickerArray objectAtIndex:0];
    }
    
    if (component == ThirdComponent) {
        self.areaString = [self.thirdPickerArray objectAtIndex:row];
    }
    
}

- (void)haveSelectedCity:(SelectedCity)tempSelectCityBlock
{
    self.selectCityBlock = tempSelectCityBlock;
}

- (void)haveCancelSelect:(CancelSelect)tempCancelSelectBlock
{
    self.cancelSelectBlock = tempCancelSelectBlock;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == FirstComponent) {
        return 90.0;
    }
    
    if (component == SubComponent) {
        return 120.0;
    }
    
    if (component == ThirdComponent) {
        return 100.0;
    }
    return 0;
}

//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
//{
//    UILabel * pickerLabel = (UILabel *)view;
//    if (!pickerLabel) {
//        pickerLabel = [[UILabel alloc] init];
//        pickerLabel.adjustsFontSizeToFitWidth = YES;
//        [pickerLabel setTextAlignment:NSTextAlignmentLeft];
//        [pickerLabel setBackgroundColor:[UIColor clearColor]];
//        [pickerLabel setFont:[UIFont systemFontOfSize:17]];
//    }
//    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
//    return pickerLabel;
//}

- (IBAction)confimClicked:(id)sender {
    
    NSString * showString = [NSString stringWithFormat:@"%@,%@,%@", self.provinceString, self.cityString, self.areaString];
    self.selectCityBlock(showString);
    
}

- (IBAction)cancelClicked:(id)sender {
    
    self.cancelSelectBlock();
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
